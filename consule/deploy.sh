#!/bin/sh

if [ ! -f ca.csr ]; then
    echo "Generate TLS Certificates"
    echo "RPC communication between each Consul member will be encrypted using TLS. Initialize a Certificate Authority (CA)"
    cfssl gencert -initca ca/ca-csr.json | cfssljson -bare ca
fi


if [ ! -f consul.csr ]; then
    echo "Generate TLS Certificates"
    echo "Create the Consul TLS certificate and private key"
    cfssl gencert \
      -ca=ca.pem \
      -ca-key=ca-key.pem \
      -config=ca/ca-config.json \
      -profile=default \
      ca/consul-csr.json | cfssljson -bare consul

    # Generate the Consul Gossip Encryption Key
    # Gossip communication between Consul members will be encrypted using a shared encryption key. Generate and store an encrypt key
    GOSSIP_ENCRYPTION_KEY=$(consul keygen)

    echo "Consul Gossip Encryption Key $GOSSIP_ENCRYPTION_KEY"
    echo "Create the Consul Secret and Configmap"
    kubectl create secret generic consul \
      --from-literal="gossip-encryption-key=${GOSSIP_ENCRYPTION_KEY}" \
      --from-file=ca.pem \
      --from-file=consul.pem \
      --from-file=consul-key.pem
fi

kubectl apply -f configmaps.yaml
kubectl apply -f services.yaml
kubectl apply -f serviceaccounts.yaml
kubectl apply -f clusterroles.yaml
kubectl apply -f statefulsets.yaml

if [ "$1" = "--port-forward" ]; then
  kubectl port-forward consul-0 8500:8500
fi


