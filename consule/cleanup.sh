#!/bin/sh

if [ "$1" = "--hard" ]; then
  echo "Delete TLS Certificates"
  rm ca-key.pem ca.csr ca.pem consul-key.pem consul.csr consul.pem
fi

kubectl delete -f statefulsets.yaml
kubectl delete -f clusterroles.yaml
kubectl delete -f serviceaccounts.yaml
kubectl delete -f services.yaml
kubectl delete -f configmaps.yaml
