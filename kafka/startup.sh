#!/bin/sh

# Applying Strimzi installation file
kubectl apply -f 'https://strimzi.io/install/latest?namespace=kafka' -n kafka

# Provision the Apache Kafka cluster
kubectl apply -f https://strimzi.io/examples/latest/kafka/kafka-persistent-single.yaml -n kafka

# We now need to wait while Kubernetes starts the required pods, services and so on:
kubectl wait kafka/my-cluster --for=condition=Ready --timeout=300s -n kafka

echo "$(kubectl get pods -n kafka)"

getPodName() {
  kubectl get pods | grep -E "$1.*" | awk '{ printf "%10s\n", $1 }'
}

kubectl apply -f topics.yaml

# kubectl port-forward $(getPodName "kafka-manager") 9021:9021

# minikube service kafka-manager
