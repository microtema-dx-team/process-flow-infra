## process-flow-infra
### create infrastructure as service and deploy to kubernetes (single node cluster) using minikube.

# create pv
# create pvc
# create secret
# create deployment
# create cm

# start minikube (single node cluster)
`minikube start --driver=hyperkit --memory=4096`
Info: `https://minikube.sigs.k8s.io/docs/drivers/hyperkit/`

# start kubernetes dashboard
Link: `minikube dashboard`
Link: `http://127.0.0.1:54134/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/#/settings?namespace=default`

# start confluent and kafka
`cd kafka && docker-compose up zookeeper kafka control-center`
# start confluent on single node
`confluent local services start`
Link: `https://docs.confluent.io/platform/current/quickstart/cos-quickstart.html`

# start consul
`cd consule && ./deploy.sh --port-forward`
