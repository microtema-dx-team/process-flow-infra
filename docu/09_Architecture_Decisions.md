# Architecture decisions

Content
Important, expensive, large scale or risky architecture decisions including rationals. With “decisions” we mean selecting one alternative based on given criteria.

Please use your judgement to decide whether an architectural decision should be documented here in this central section or whether you better document it locally (e.g. within the white box template of one building block). Avoid redundant texts. Refer to section 4, where you captured the most important decisions of your architecture already.

Motivation
Stakeholders of your system should be able to comprehend and retrace your decisions.

Form
list or table, ordered by importance and consequences or
more detailed in form of separate sections per decision
ADR (architecture decision record) for every important decision

| Decisions | explanations | Explanations |
| ---------  | ----------- | ------- |
| Cloud Platform | Kubernetes | ... |
| Service Discovery   | Consul | ... |
| Message Broker   | Apache Kafka | ... |
| Process Engine   | Camunda | ... |
| Database   | Postgresql | ... |
| Programming Language   | Java, Javascript, Groovy | ... |
| Backend Frameworks  | Spring Boot  | ... |
| Frontend Frameworks  | VueJs | ... |
| CI Server  | Bitbucket | ... |
| SCM Server  | Bitbucket | ... |
| Build Tools  | Maven, NPM | ... |
