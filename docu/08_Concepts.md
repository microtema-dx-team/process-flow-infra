# Crosscutting Concepts

The form can be varied:

concept papers with any kind of structure
cross-cutting model excerpts or scenarios using notations of the architecture views
sample implementations,especially for technical concepts
reference to typical usage of standard frameworks (e.g. using Hibernate for object/relational mapping)
Structure of this Section
A potential (but not mandatory) structure for this section could be:

Domain concepts
User Experience concepts (UX)
Safety and security concepts
Architecture and design patterns
“Under-the-hood” concepts
Development concepts
Operational concepts
Note: it might be difficult to assign individual concepts to one specific topic on this list.


![Scope and context](images/08-Crosscutting-Concepts.png)

# Architecture & Design Patterns

An architectural pattern is a general, reusable solution to a commonly occurring problem in software architecture within a given context. … 
The architectural patterns address various issues in software engineering, 
such as computer hardware performance limitations, high availability and minimization of a business risk.

## Architecture
* Architecture is the overall structure of software.
* Architecture is the structure of the software system in its entirety.
* Developer chooses different design pattern according to the architecture specification and requirement.
* It’s define the granularity of the component.

### Layered pattern

We will use The most commonly found 4 layers of a general information system
* Presentation layer (also known as UI layer)
* Application layer (also known as service layer)
* Business logic layer (also known as domain layer)
* Data access layer (also known as persistence layer)

### Client-server pattern

This pattern consists of two parties; a server and multiple clients. The server component will provide services to multiple client components. 
Clients request services from the server and the server provides relevant services to those clients. 
Furthermore, the server continues to listen to client requests.

### Pipe-filter pattern

This pattern can be used to structure systems which produce and process a stream of data. Each processing step is enclosed within a filter component. Data to be processed is passed through pipes. 
These pipes can be used for buffering or for synchronization purposes.

### Broker pattern

This pattern is used to structure distributed systems with decoupled components. These components can interact with each other by remote service invocations. A broker component is responsible for the coordination of communication among components.
Servers publish their capabilities (services and characteristics) to a broker. Clients request a service from the broker, and the broker then redirects the client to a suitable service from its registry.

### Event-bus pattern

This pattern primarily deals with events and has 4 major components; 
event source, event listener, channel and event bus. 
Sources publish messages to particular channels on an event bus. 
Listeners subscribe to particular channels. 
Listeners are notified of messages that are published to a channel to which they have subscribed before.

### Model-view-controller pattern

This pattern, also known as MVC pattern, divides an interactive application in to 3 parts as,
* model — contains the core functionality and data
* view — displays the information to the user (more than one view may be defined)
* controller — handles the input from the user
This is done to separate internal representations of information from the ways information is presented to, and accepted from, the user. It decouples components and allows efficient code reuse.

[more...](https://towardsdatascience.com/10-common-software-architectural-patterns-in-a-nutshell-a0b47a1e9013)

## Design pattern:
* Design patterns are concerned with how the components are built.
* It’s about particular solution.

### Differences :
* Architecture comes in Designing phase and Design Patterns comes in Building phase.
* Architectural pattern is like a blue print and design pattern is actual implementation.
* Architecture is base which everything else adhere to and design pattern is a way to structure classes to solve common problems.
* All Architecture is design pattern but all design pattern can not be architecture. Like MVC can come under both. 
  But singleton design pattern can not be an architecture pattern. MVC, MVVM all come under both.
* Architecture : how components should behave and communicate in the system, set the physical location of components and finally choose the tools in order to create components. 
  Design : while architecture deals more with the wide picture, design should drill down into the details relating to implementing certain components. 
  Designing of components end up with classes, interfaces, abstract classes and other OO feature in order to fulfil the given component tasks.
  
### SOLID
* single responsibility principle
* open/closed principle
* liskov substitution principle
* interface segregation principle
* dependency inversion

[more...](https://dzone.com/articles/the-solid-principles-in-real-life)

# User Experience (UX)

## User Interface

## Ergonomics

## Internalization (i18n)

# Safety & Security

> Safety and security are two essential aspects of systems and software.

## Safety 
  is the freedom from unacceptable risk or harm. Safety is generally thought of in terms of data integrity. 
  Backups, checksums, etc all ensure that the data is safe from failure.

## Security
  may be defined as the prevention of illegal or unwanted interference with the intended operation or inappropriate access of a system.
  Security is about ensuring our software functions correctly while under attack and we do this with great measures, by practicing good programming techniques such as validating input from untrusted data sources. 
  It is important to note that security is highly focused on the deliberate actions that are geared towards inflicting harm to an individual, organization, or even assets.

### Software security (pre-deployment) activities include:

* Secure software design
* Development of secure coding guidelines for developers to follow
* Development of secure configuration procedures and standards for the deployment phase
* Secure coding that follows established guidelines
* Validation of user input and implementation of a suitable encoding strategy
* User authentication
* User session management
* Function level access control
* Use of strong cryptography to secure data at rest and in transit
* Validation of third-party components
* Arrest of any flaws in software design/architecture

### Application security (post-deployment) activities include:

* Post deployment security tests
* Capture of flaws in software environment configuration
* Malicious code detection (implemented by the developer to create backdoor, time bomb)
* Patch/upgrade
* IP filtering
* Lock down executables
* Monitoring of programs at runtime to enforce the software use policy

### OWASP Top 10 Security Risks & Vulnerabilities

* Injection
* Broken Authentication
* Sensitive Data Exposure
* XML External Entities (XXE)
* Broken Access control
* Security misconfigurations
* Cross Site Scripting (XSS)
* Insecure Deserialization
* Using Components with known vulnerabilities
* Insufficient logging and monitoring

[more](https://sucuri.net/guides/owasp-top-10-security-vulnerabilities-2020/)

# Development 

## Build, Test and Deploy

## Code generation

## Migration

> Flyway Db Migration Tool

## Configurability

# Under the hood

## Persistency

## Transaction Handling

## Exception and Error Handling

## Communication and Integration

## Parallelization / Threading

## Checks & Validation

## Business Rules

## Batch

## Reporting

# Operation

## Administration

## Disaster-Recovery

## Scalability

## Clustering

## Monitoring & Logging

## High Availability
