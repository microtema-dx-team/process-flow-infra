# Glossary

Content
The most important domain and technical terms that your stakeholders use when discussing the system.

You can also see the glossary as source for translations if you work in a multi-language environment (i.e. in offshore development models).

Motivation
You should clearly define your terms, so that all stakeholders

have an identical understanding of these terme
do not use synonyms and homonyms
Form
A simple table with columns <Term> and <Definition>
Potentially more columns for translations

| Term | Definition | 
| ---  | ---- |
| PRFL | Process Flow |
| Context | The setting in which a word or statement appears that determines its meaning |
| DOMAIN | A sphere of knowledge (ontology), influence, or activity. The subject area to which the user applies a program is the domain of the software |
| Model | A system of abstractions that describes selected aspects of a domain and can be used to solve problems related to that domain |

