# Building block view

Content
The building block view shows the static decomposition of the system into building blocks (modules, components, subsystems, classes, interfaces, packages, libraries, frameworks, layers, partitions, tiers, functions, macros, operations, datas structures, …) as well as their dependencies (relationships, associations, …)

This view is mandatory for every architecture documentation. In analogy to a house this is the floor plan.

Motivation
Maintain an overview of your source code by making its structure understandable through abstraction.

This allows you to communicate with your stakeholder on an abstract level without disclosing implementation details.

Form
The building block view is a hierarchial collection of black boxes and white boxes (see figure below) and their descriptions.

## Whitebox Overall System
![Scope and context](images/scope-and-context.png)

### Scope and Context

is the white box description of the overall system together with black box descriptions of all contained building blocks.

| Name | Responsibility |
| ---  | ------- |
| Platform | .... |
| Infra | .... |
| Domain | .... |

### Platform

zooms into some building blocks of level 1. Thus it contains the white box description of selected building blocks of level 1, together with black box descriptions of their internal building blocks.

![Scope and context](images/platform.png)

| Name | Responsibility |
| ---  | ------- |
| Database | .... |
| Backend | .... |
| Frontend | .... |

### Infra

zooms into some building blocks of level 1. Thus it contains the white box description of selected building blocks of level 1, together with black box descriptions of their internal building blocks.

![Scope and context](images/infra.png)

| Name | Responsibility |
| ---  | ------- |
| Message Broker | .... |
| Service Discovery | .... |

### Domain

zooms into some building blocks of level 1. Thus it contains the white box description of selected building blocks of level 1, together with black box descriptions of their internal building blocks.

![Scope and context](images/domain.png)

| Name | Responsibility |
| ---  | ------- |
| Database | .... |
| Process Engine | .... |
| Backend | .... |
| Frontend | .... |

## Data View

![Data View](images/05_Data.png)
