# Constraints

Content
Any requirement that constrains software architects in their freedom of design and implementation decisions or decision about the development process. These constraints sometimes go beyond individual systems and are valid for whole organizations and companies.

Motivation
Architects should know exactly where they are free in their design decisions and where they must adhere to constraints. Constraints must always be dealt with; they may be negotiable, though.

Form
Simple tables of constraints with explanations. If needed you can subdivide them into technical constraints, organizational and political constraints and conventions (e.g. programming or versioning guidelines, documentation or naming conventions)

| Constraint | explanations | Explanations |
| ---------  | ----------- | ------- |
| Cloud Platform | Kubernetes | ... |
| Service Discovery   | Consul | ... |
| Message Broker   | Apache Kafka | ... |
| Process Engine   | Camunda | ... |
| Database   | Postgresql | ... |
| Programming Language   | Java, Javascript, Groovy | ... |
| Backend Frameworks  | Spring Boot  | ... |
| Frontend Frameworks  | VueJs | ... |
| CI Server  | Bitbucket | ... |
| SCM Server  | Bitbucket | ... |
| Build Tools  | Maven, NPM | ... |
