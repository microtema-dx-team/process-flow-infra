# Runtime view

Content
The runtime view describes concrete behavior and interactions of the system’s building blocks in form of scenarios from the following areas:

important use cases or features: how do building blocks execute them?
interactions at critical external interfaces: how do building blocks cooperate with users and neighbouring systems?
operation and administration: launch, start-up, stop
error and exception scenarios
Remark: The main criterion for the choice of possible scenarios (sequences, workflows) is their architectural relevancy. It is not important to describe a large number of scenarios. You should rather document a representative selection.

Motivation
You should understand how (instances of) building blocks of your system perform their job and communicate at runtime. You will mainly capture scenarios in your documentation to communicate your architecture to stakholders that are less willing or able to read and understand the static models (building block view, deployment view).

Form
There are many notations for describing scenarios, e.g.

numbered list of steps (in natural language)
activity diagrams or flow charts
sequence diagrams
BPMN or EPCs (event process chains)
state machines

# Register Process

![Register Process](images/06_register-process.png)

# Start Process

![Start Process](images/06_start-process.png)

# Execute Task

![Start Process](images/06_execute-task.png)
