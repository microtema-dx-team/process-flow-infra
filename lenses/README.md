# Start

## License ID:
58fb7b6c-83d6-11eb-9025-42010af01003

docker run -e ADV_HOST=127.0.0.1 -e EULA="https://licenses.lenses.io/download/lensesdl?id=58fb7b6c-83d6-11eb-9025-42010af01003" --rm -p 3030:3030 -p 9092:9092 lensesio/box

# Access from your browser
http://localhost:3030

# Credentials
Username:   admin 
Pwd:        admin
