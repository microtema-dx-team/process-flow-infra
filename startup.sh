#!/bin/sh

minikube start --memory=6144 # 2GB default memory isn't always enough

echo "minikube ip: $(minikube ip)"

kubectl apply -f namespace.yaml
kubectl apply -f pv.yaml


chmod +x lenses/startup.sh
cd lenses || exit
# ./startup.sh
cd ..

chmod +x platform/startup.sh
cd platform || exit
./startup.sh
cd ..

chmod +x domain/startup.sh
cd domain || exit
./startup.sh
cd ..


echo "minikube ip: $(minikube ip)"

# start dashboard
minikube dashboard
